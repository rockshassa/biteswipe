//
//  MenuCategory.h
//  BiteSwipe
//
//  Created by Niko on 4/29/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuCategory : NSObject

@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSArray *items;

@end
