//
//  ReadMenuOperation.m
//  BiteSwipe
//
//  Created by Niko on 4/29/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import "ReadMenuOperation.h"
#import "ItemAttribute.h"
#import "MenuCategory.h"
#import "MenuItem.h"

@implementation ReadMenuOperation

-(void)main{
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"opti_JSON" ofType:@"json"];
    NSData *data = [[NSData alloc] initWithContentsOfFile:filePath];
    
    // Parse the string into JSON
    if (data){
        NSError *jsonError;
        NSDictionary *jsonObjects = [NSJSONSerialization JSONObjectWithData:data
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
//        NSLog(@"%@", jsonObjects.description);
        
        NSMutableArray *itemsArray = [jsonObjects objectForKey:@"itemCategories"];
        
        NSMutableArray *itemCategories = [[NSMutableArray alloc] initWithCapacity:10];
        
        for (NSMutableDictionary *categoryDict in itemsArray) {
            
            //get category
            MenuCategory *category = [[MenuCategory alloc] init];
            category.displayName = [categoryDict objectForKey:@"DisplayName"];
            
            //get items for category
            NSMutableArray *items = [categoryDict objectForKey:@"items"];
            
            NSMutableArray *catItems = [[NSMutableArray alloc] initWithCapacity:1];
            
            for (NSMutableDictionary *itemDict in items) {
                
                MenuItem *item = [[MenuItem alloc] init];
                
                item.displayName = [itemDict objectForKey:@"DisplayName"];
                item.value = [itemDict objectForKey:@"value"];
                
                //get attributes of this item.
                NSMutableDictionary *attributesDict = [itemDict objectForKey:@"Attributes"];
                
                NSMutableArray *attribArray = [[NSMutableArray alloc] initWithCapacity:1];
                
                //attribute logic
                for (NSString *key in attributesDict) {
                    
                    ItemAttribute *itemAttrib = [[ItemAttribute alloc] init];
                    
                    itemAttrib.displayName = key;
                    
                    NSMutableDictionary *aD = [attributesDict objectForKey:key];
                    
                    NSMutableDictionary *propertyDict = [aD objectForKey:@"Properties"];
                    itemAttrib.increments = (BOOL)[propertyDict valueForKey:@"increments"];
                    
                    itemAttrib.quanititiesDict = [aD objectForKey:@"Quantities"];
                    itemAttrib.valuesDict = [aD objectForKey:@"Values"];
                    
                    [attribArray addObject:itemAttrib];
                    
                }
                item.attributes = attribArray;
                
                [catItems addObject:item];
            }
            
            category.items = catItems;
            
            [itemCategories addObject:category];
            
        }
        
        NSLog(@"%@",itemCategories);
    }
}

@end
