//
//  ItemAttribute.h
//  BiteSwipe
//
//  Created by Niko on 4/29/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemAttribute : NSObject

@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSDictionary *valuesDict;
@property (nonatomic, strong) NSDictionary *quanititiesDict;
@property (nonatomic, assign) BOOL increments;


@end
