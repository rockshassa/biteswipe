//
//  MenuItem.h
//  BiteSwipe
//
//  Created by Niko on 4/29/13.
//  Copyright (c) 2013 Rockshassa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuItem : NSObject

@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSArray *attributes;
@property (nonatomic, strong) NSString *value;

@end
