// This file has been changed from original state.
// test
//
//  ViewController.m
//  QREncoderProject
//
//  Created by Daniel Beard on 5/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "QRCodeViewController.h"

void freeRawData(void *info, const void *data, size_t size);

@interface QRCodeViewController ()

@property (nonatomic, strong) UIImageView *codeView;

@end

@implementation QRCodeViewController

-(id)init{
    
    self = [super initWithNibName:nil bundle:nil];
    if (self){
        
        self.title = @"QRCode";
    }
    
    return self;
}

#pragma mark - View lifecycle

-(void)loadView{
    
    UIView *incView = [[UIView alloc] initWithFrame:self.navigationController.view.bounds];
    incView.backgroundColor = [UIColor orangeColor];
    
    NSString *delimiter = @"\%0D\%0A";
    
    NSString *codeString = [NSString stringWithFormat:@"025000046025%@049000003710%@881334004533%@", delimiter, delimiter, delimiter];
    NSLog(@"%@", codeString);
    
    UIImage *image = [self quickResponseImageForString:codeString withDimension:320];
    
    _codeView = [[UIImageView alloc] initWithImage:image];
    
    [incView addSubview:_codeView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 320, incView.frame.size.width, incView.frame.size.height- image.size.height)];
    textLabel.numberOfLines = 0;
    textLabel.text = [NSString stringWithFormat:@"(slash 0D0A)\nMM Orange\nFP Powerade\nK cup Original\n%@", codeString];
    [textLabel sizeToFit];
    
    CGRect frame = textLabel.frame;
    frame.origin = CGPointMake(0, 320);
    textLabel.frame = frame;
    
    [incView addSubview:textLabel];
    
    self.view = incView;
    
}

void freeRawData(void *info, const void *data, size_t size) {
    free((unsigned char *)data);
}

- (UIImage *)quickResponseImageForString:(NSString *)dataString withDimension:(int)imageWidth {
    
    QRcode *resultCode = QRcode_encodeString([dataString UTF8String], 0, QR_ECLEVEL_L, QR_MODE_8, 1);
    
    unsigned char *pixels = (*resultCode).data;
    int width = (*resultCode).width;
    int len = width * width;
    
    if (imageWidth < width)
        imageWidth = width;
    
    // Set bit-fiddling variables
    int bytesPerPixel = 4;
    int bitsPerPixel = 8 * bytesPerPixel;
    int bytesPerLine = bytesPerPixel * imageWidth;
    int rawDataSize = bytesPerLine * imageWidth;
    
    int pixelPerDot = imageWidth / width;
    int offset = (int)((imageWidth - pixelPerDot * width) / 2);
    
    // Allocate raw image buffer
    unsigned char *rawData = (unsigned char*)malloc(rawDataSize);
    memset(rawData, 0xFF, rawDataSize);
    
    // Fill raw image buffer with image data from QR code matrix
    int i;
    for (i = 0; i < len; i++) {
        char intensity = (pixels[i] & 1) ? 0 : 0xFF;
        
        int y = i / width;
        int x = i - (y * width);
        
        int startX = pixelPerDot * x * bytesPerPixel + (bytesPerPixel * offset);
        int startY = pixelPerDot * y + offset;
        int endX = startX + pixelPerDot * bytesPerPixel;
        int endY = startY + pixelPerDot;
        
        int my;
        for (my = startY; my < endY; my++) {
            int mx;
            for (mx = startX; mx < endX; mx += bytesPerPixel) {
                rawData[bytesPerLine * my + mx    ] = intensity;    //red
                rawData[bytesPerLine * my + mx + 1] = intensity;    //green
                rawData[bytesPerLine * my + mx + 2] = intensity;    //blue
                rawData[bytesPerLine * my + mx + 3] = 255;          //alpha
            }
        }
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, rawData, rawDataSize, (CGDataProviderReleaseDataCallback)&freeRawData);
    CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
    CGImageRef imageRef = CGImageCreate(imageWidth, imageWidth, 8, bitsPerPixel, bytesPerLine, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
    
    UIImage *quickResponseImage = [UIImage imageWithCGImage:imageRef];
    
    CGImageRelease(imageRef);
    CGColorSpaceRelease(colorSpaceRef);
    CGDataProviderRelease(provider);
    QRcode_free(resultCode);
    
    return quickResponseImage;
}


@end
